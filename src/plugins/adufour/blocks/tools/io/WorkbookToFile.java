package plugins.adufour.blocks.tools.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import icy.file.FileUtil;
import icy.plugin.abstract_.Plugin;
import icy.system.IcyHandledException;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarEnum;
import plugins.adufour.vars.lang.VarFile;
import plugins.adufour.vars.lang.VarWorkbook;
import plugins.adufour.workbooks.Workbooks;

/**
 * IO block that writes a workbook to a file using the POI library
 * 
 * @author Alexandre Dufour
 */
public class WorkbookToFile extends Plugin implements IOBlock
{
    public enum WorkbookFormat
    {
        /** A format compatible with most spreadsheet software (~ XLS) */
        Spreadsheet,
        
        /** A tab-delimited text format */
        Text
    }
    
    public enum MergePolicy
    {
        /** File is overwritten entirely */
        Overwrite,
        
        /** New sheets are merged into existing ones if they have the same name */
        Merge_sheets,
        
        /**
         * New sheets are merged into existing ones if they have the same name, but the first
         * (header) row is not copied
         */
        Merge_sheets___excluding_first_row;
        
        public String toString()
        {
            return super.toString().replace("__", ",").replace("_", " ");
        }
    }
    
    VarWorkbook             workbook = new VarWorkbook("workbook", (Workbook) null);
    VarFile                 file     = new VarFile("output file", null);
    VarEnum<WorkbookFormat> format   = new VarEnum<WorkbookFormat>("file format", WorkbookFormat.Spreadsheet);
    VarEnum<MergePolicy>    append   = new VarEnum<MergePolicy>("If file exists", MergePolicy.Overwrite);
    
    @Override
    public void run()
    {
        switch (format.getValue())
        {
        case Spreadsheet:
            saveAsSpreadSheet(workbook.getValue(true), file.getValue(true).getAbsolutePath(), append.getValue());
            break;
        case Text:
            saveAsText(workbook.getValue(true), file.getValue(true).getAbsolutePath(), append.getValue());
            break;
        }
        
        if (append.getValue() != MergePolicy.Overwrite) workbook.valueChanged(workbook, null, workbook.getValue());
    }
    
    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("workbook", workbook);
        inputMap.add("file format", format);
        inputMap.add("output file", file);
        inputMap.add("If file exists", append);
    }
    
    @Override
    public void declareOutput(VarList outputMap)
    {
    }
    
    /**
     * Saves the specified workbook to a spreadsheet file. By default, the ".xls" extension will be
     * added to the file name if it is not present
     * 
     * @param workbook
     * @param fileName
     * @deprecated Use {@link #saveAsSpreadSheet(Workbook, String, MergePolicy)} instead.
     */
    @Deprecated
    public static void saveAsSpreadSheet(Workbook workbook, String fileName)
    {
        saveAsSpreadSheet(workbook, fileName, MergePolicy.Overwrite);
    }
    
    /**
     * Saves the specified workbook to a spreadsheet file. By default, the ".xls" extension will be
     * added to the file name if it is not present
     * 
     * @param workbook
     * @param fileName
     */
    public static void saveAsSpreadSheet(Workbook workbook, String fileName, MergePolicy mergePolicy)
    {
        if (new File(fileName).isDirectory()) throw new IcyHandledException("Cannot save workbook: the specified file is a folder");
        
        File file = new File(fileName);
        
        // Add a default extension (if necessary)
        String extension = Workbooks.getFormat(workbook).getExtension();
        if (!file.exists() && !fileName.toLowerCase().endsWith(extension)) file = new File(fileName + extension);
        
        // should we merge?
        if (file.exists() && mergePolicy != MergePolicy.Overwrite) try
        {
            Workbook wb = FileToWorkbook.readWorkbook(file);
            mergeWorkbooks(workbook, wb, mergePolicy);
            workbook = wb;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        
        try
        {
            File tmp = File.createTempFile("icy_tmp_workbook", "xls");
            FileOutputStream stream = new FileOutputStream(tmp);
            workbook.write(stream);
            workbook.close();
            stream.close();
            FileUtil.rename(tmp, file, true);
        }
        catch (FileNotFoundException e)
        {
            String error = "Cannot create file " + fileName + "\n";
            error += "Reason: " + e.getMessage() + "\n\n";
            error += "Please ensure that:\n";
            error += " - You have permission to write to this folder\n";
            error += " - The file name does not contains any special characters";
            
            throw new IcyHandledException(error);
        }
        catch (IOException e)
        {
            String error = "Cannot write file " + fileName + "\n";
            error += "Reason: " + e.getMessage() + "\n\n";
            error += "Please ensure that the file is not opened with another application.";
            
            throw new IcyHandledException(error);
        }
    }
    
    /**
     * Saves the specified workbook to a spreadsheet file. By default, the ".txt" extension will be
     * added to the file name if it is not present
     * 
     * @param workbook
     * @param fileName
     * @deprecated Use {@link #saveAsText(Workbook, String, MergePolicy)} instead.
     */
    @Deprecated
    public static void saveAsText(Workbook workbook, String fileName)
    {
        saveAsText(workbook, fileName, MergePolicy.Overwrite);
    }
    
    /**
     * Saves the specified workbook to a spreadsheet file. By default, the ".txt" extension will be
     * added to the file name if it is not present. If the workbook contains multiple sheets, they
     * will be stored in the text file as in the following example:<br/>
     * <code>== sheet 1 ==<br/>
     * item1 (tab) item2<br/>
     * <br/>
     * == sheet 2 ==<br/>
     * item3 (tab) item4</code><br/>
     * 
     * @param workbook
     *            the workbook to save
     * @param fileName
     *            the name of the file to save to
     * @param mergePolicy
     *            indicates if and how the file should be appended
     */
    public static void saveAsText(Workbook workbook, String fileName, MergePolicy mergePolicy)
    {
        File file = new File(fileName);
        
        if (file.isDirectory()) throw new IcyHandledException("Cannot save workbook: the specified file is a folder");
        
        // Add a default extension (if necessary)
        if (!file.exists() && !fileName.toLowerCase().endsWith(".txt")) file = new File(fileName + ".txt");
        
        // should we merge?
        if (file.exists() && mergePolicy != MergePolicy.Overwrite) try
        {
            Workbook wb = FileToWorkbook.readWorkbook(file);
            mergeWorkbooks(workbook, wb, mergePolicy);
            workbook = wb;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        
        try
        {
            FileWriter fw = new FileWriter(file);
            
            int n = workbook.getNumberOfSheets();
            
            for (int i = 0; i < n; i++)
            {
                Sheet sheet = workbook.getSheetAt(i);
                
                fw.write("== " + sheet.getSheetName() + " ==\n");
                
                for (int r = sheet.getFirstRowNum(); r <= sheet.getLastRowNum(); r++)
                {
                    Row row = sheet.getRow(r);
                    
                    if (row != null)
                    {
                        for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++)
                        {
                            Cell cell = row.getCell(c, Row.RETURN_BLANK_AS_NULL);
                            if (cell != null) fw.write(cell.toString());
                            fw.write("\t");
                        }
                    }
                    fw.write("\n");
                }
                
                fw.write("\n");
            }
            
            fw.flush();
            fw.close();
        }
        catch (FileNotFoundException e)
        {
            String error = "Cannot create file " + fileName + "\n";
            error += "Reason: " + e.getMessage() + "\n\n";
            error += "Please ensure that:\n";
            error += " - You have permission to write to this folder\n";
            error += " - The file name does not contains any special characters";
            
            throw new IcyHandledException(error);
        }
        catch (IOException e)
        {
            String error = "Cannot write file " + fileName + "\n";
            error += "Reason: " + e.getMessage() + "\n\n";
            error += "Please ensure that the file is not opened with another application.";
            
            throw new IcyHandledException(error);
        }
    }
    
    /**
     * @param source
     * @param target
     * @param policy
     * @return
     */
    private static void mergeWorkbooks(Workbook source, Workbook target, MergePolicy policy)
    {
        if (policy == MergePolicy.Overwrite) return;
        
        // case Add_new_sheets:
        // for (int i = 0; i < source.getNumberOfSheets(); i++)
        // {
        // Sheet sourceSheet = source.getSheetAt(i);
        // // make sure the name is unique
        // String sheetName = sourceSheet.getSheetName();
        // // if not, add underscores
        // while (target.getSheet(sheetName) != null)
        // sheetName = "_" + sheetName;
        //
        // // create the new sheet
        // Sheet targetSheet = target.createSheet(sheetName);
        // // copy everything manually (sigh)
        // for (int r = 0; r <= sourceSheet.getLastRowNum(); r++)
        // {
        // Row sourceRow = sourceSheet.getRow(r);
        // if (sourceRow == null) continue;
        // Row targetRow = targetSheet.createRow(r);
        // for (Cell sourceCell : sourceRow)
        // {
        // // Copy the cell
        // Cell targetCell = targetRow.createCell(sourceCell.getColumnIndex());
        // CellStyle style = target.createCellStyle();
        // style.cloneStyleFrom(sourceCell.getCellStyle());
        // targetCell.setCellStyle(style);
        // targetCell.setCellType(sourceCell.getCellType());
        // switch (sourceCell.getCellType())
        // {
        // case Cell.CELL_TYPE_BOOLEAN:
        // targetCell.setCellValue(sourceCell.getBooleanCellValue());
        // break;
        // case Cell.CELL_TYPE_FORMULA:
        // targetCell.setCellFormula(sourceCell.getCellFormula());
        // break;
        // case Cell.CELL_TYPE_NUMERIC:
        // targetCell.setCellValue(sourceCell.getNumericCellValue());
        // break;
        // case Cell.CELL_TYPE_STRING:
        // targetCell.setCellValue(sourceCell.getStringCellValue());
        // break;
        // case Cell.CELL_TYPE_ERROR:
        // targetCell.setCellErrorValue(sourceCell.getErrorCellValue());
        // break;
        // default:
        // }
        // }
        // }
        // }
        
        HashMap<Integer, CellStyle> styles = new HashMap<Integer, CellStyle>();
        
        for (int i = 0; i < source.getNumberOfSheets(); i++)
        {
            Sheet sourceSheet = source.getSheetAt(i);
            String sourceSheetName = sourceSheet.getSheetName();
            
            // does the sheet exist in the target workbook?
            Sheet targetSheet = target.getSheet(sourceSheetName);
            
            int sourceRowNum = 0;
            if (targetSheet == null)
            {
                targetSheet = target.createSheet(sourceSheetName);
            }
            else if (policy == MergePolicy.Merge_sheets___excluding_first_row)
            {
                sourceRowNum++;
            }
            
            int targetRowNum = targetSheet.getLastRowNum() + 1;
            
            for (int r = sourceRowNum; r <= sourceSheet.getLastRowNum(); r++, targetRowNum++)
            {
                Row sourceRow = sourceSheet.getRow(r);
                if (sourceRow == null) continue;
                Row targetRow = targetSheet.createRow(targetRowNum);
                for (Cell sourceCell : sourceRow)
                {
                    // Copy the cell
                    Cell targetCell = targetRow.createCell(sourceCell.getColumnIndex());
                    int hashCode = sourceCell.getCellStyle().hashCode();
                    if (!styles.containsKey(hashCode))
                    {
                        CellStyle targetStyle = target.createCellStyle();
                        targetStyle.cloneStyleFrom(sourceCell.getCellStyle());
                        styles.put(hashCode, targetStyle);
                    }
                    targetCell.setCellStyle(styles.get(hashCode));
                    targetCell.setCellType(sourceCell.getCellType());
                    switch (sourceCell.getCellType())
                    {
                    case Cell.CELL_TYPE_BOOLEAN:
                        targetCell.setCellValue(sourceCell.getBooleanCellValue());
                        break;
                    case Cell.CELL_TYPE_FORMULA:
                        targetCell.setCellFormula(sourceCell.getCellFormula());
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        targetCell.setCellValue(sourceCell.getNumericCellValue());
                        break;
                    case Cell.CELL_TYPE_STRING:
                        targetCell.setCellValue(sourceCell.getStringCellValue());
                        break;
                    case Cell.CELL_TYPE_ERROR:
                        targetCell.setCellErrorValue(sourceCell.getErrorCellValue());
                        break;
                    default:
                    }
                }
            }
        }
    }
}
