package plugins.adufour.vars.gui.swing;

import java.util.List;

import javax.swing.Action;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.MenuSelectionManager;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.action.AbstractActionExt;
import org.jdesktop.swingx.action.ActionContainerFactory;
import org.jdesktop.swingx.table.ColumnControlButton;
import org.jdesktop.swingx.table.ColumnControlPopup;
import org.pushingpixels.substance.internal.ui.SubstanceCheckBoxMenuItemUI;

/**
 * Persistent version of JXTable's column control button that keeps the pop-up open even after a
 * column is clicked (allowing multiple clicks at once). The pop-up will close if the
 * ColumnControlButton is pressed again. <br/>
 * <br/>
 * This code was adapted from the <a
 * href="http://www.coding-dude.com/wp/java/custom-columncontrolbutton-java-swing-jxtable/">
 * following page</a>.
 * 
 * @author John Negoita
 * @author Alexandre Dufour
 */
@SuppressWarnings("serial")
public class PersistentColumnControlButton extends ColumnControlButton
{
    public PersistentColumnControlButton(JXTable table)
    {
        super(table);
    }
    
    @Override
    protected ColumnControlPopup createColumnControlPopup()
    {
        return new DefaultColumnControlPopup()
        {
            @Override
            public void addVisibilityActionItems(List<? extends AbstractActionExt> actions)
            {
                
                ActionContainerFactory factory = new ActionContainerFactory(null);
                for (Action action : actions)
                {
                    JMenuItem mi = factory.createMenuItem(action);
                    mi.setUI(new MyCheckBoxMenuItemUI((JCheckBoxMenuItem) mi));
                    addItem(mi);
                }
            }
            
        };
    }
    
    private static class MyCheckBoxMenuItemUI extends SubstanceCheckBoxMenuItemUI
    {
        public MyCheckBoxMenuItemUI(JCheckBoxMenuItem menuItem)
        {
            super(menuItem);
        }
        
        synchronized protected void doClick(MenuSelectionManager msm)
        {
            menuItem.doClick(0);
        }
    }
}